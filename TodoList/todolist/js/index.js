const BASE_URL = "https://62de511fccdf9f7ec2d50a65.mockapi.io";

function getDscv() {
  openLoading();
  axios({
    url: `${BASE_URL}/tasks`,
    method: "GET",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      renderDscv(res.data);
      dscv = res.data;
      document.getElementById("addItem").style.display = "block";
      document.getElementById("updateItem").style.display = "none";
      getDscvHoanThanh();
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}
getDscv();

function getDscvHoanThanh() {
  openLoading();
  axios({
    url: `${BASE_URL}/completeTasks`,
    method: "GET",
  })
    .then(function (res) {
      renderCompleteTasks(res.data);
      closeLoading();
      console.log(res);
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}

var dscv = [];
var completeTasks = [];
var tempId = "";

function themCv() {
  var tenCv = document.getElementById("newTask").value;

  openLoading();
  axios({
    url: `${BASE_URL}/tasks`,
    method: "POST",
    data: { name: tenCv },
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      getDscv();
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
  document.getElementById("newTask").value = "";
}

function xoaCv(id) {
  axios({
    url: `${BASE_URL}/tasks/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      getDscv();
      document.getElementById("newTask").value = "";
    })
    .catch(function (err) {
      console.log(err);
    });
}

function suaCv(id) {
  openLoading();
  axios({
    url: `${BASE_URL}/tasks/${id}`,
    method: "GET",
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      tempId = res.data.id;
      document.getElementById("newTask").value = res.data.name;
      document.getElementById("addItem").style.display = "none";
      document.getElementById("updateItem").style.display = "block";
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}

function capNhatCv() {
  var tenCv = document.getElementById("newTask").value;

  openLoading();
  axios({
    url: `${BASE_URL}/tasks/${tempId}`,
    method: "PUT",
    data: { name: tenCv },
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
      getDscv();
      document.getElementById("newTask").value = "";
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });
}

function hoanThanhCv(id) {
  var index = dscv.findIndex((cv) => cv.id == id);
  var completeTask = dscv[index];
  completeTasks.push(completeTask);

  xoaCv(id);
  getDscv();

  openLoading();
  axios({
    url: `${BASE_URL}/completeTasks`,
    method: "POST",
    data: { name: completeTask.name },
  })
    .then(function (res) {
      closeLoading();
      console.log(res);
    })
    .catch(function (err) {
      closeLoading();
      console.log(err);
    });

  getDscvHoanThanh();
}
